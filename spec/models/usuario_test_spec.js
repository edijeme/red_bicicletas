var mongoose = require('mongoose');
var Usuario = require('../../models/usuarioMdl');
var Reserva = require('../../models/reservaMdl');
var Bicicleta = require('../../models/bicicletaMdl');

describe('Testing Reserva', function () {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('Conexion Exitosa a la base de datos test');
            done();
        });
    });

    afterEach(function (done) {
        Reserva.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function (err, success) {
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function (err, success) {
                    if (err) console.log(err);
                    done();
                });
                done();
            });
            done();
        });
    });

    describe("Cuando un Usuario reserva una bici", () => {
        it("debe existir la reserva", (done) => {
            const usuario = new Usuario({ nombre: "Pedro" });
            usuario.save();
            const bicicleta = new Bicicleta({ code: 1, color: "Roja", modelo: "4X4" });
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate() + 1);
            usuario.reservar(bicicleta.id, hoy, mañana, function (err, reserva) {
                Reserva.find({}).populate("bicicleta").populate("usuario").exec(function (err, reservas) {
                    console.log("------------");
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });

});