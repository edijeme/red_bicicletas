var mongoose = require('mongoose');
var Bici = require('../../models/bicicletaMdl');

describe('Testing Bicicletas', function () {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('Conexion Exitosa a la base de datos test');
            done();
        });
    });

    afterEach(function (done) {
        Bici.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('Bici.createInstance', () => {
        it('Crear instacia bicileta', (done) => {
            var data = Bici.createInstance(1, "Rojo", "Urbana", [10.46, -73.24]);
            expect(data.code).toBe(1);
            expect(data.color).toBe("Rojo");
            expect(data.modelo).toBe("Urbana");
            expect(data.ubicacion[0]).toEqual(10.46);
            expect(data.ubicacion[1]).toEqual(-73.24);
            done();
        });
    });

    describe('Bici.allBicis', () => {
        it('comienza vacia', (done) => {
            Bici.allBicis(function (err, bici) {
                expect(bici.length).toBe(0);
                done();
            });
        });
    });

    describe('Bici.add', () => {
        it('agregar una bici', (done) => {
            var help = new Bici({ code: 1, color: "verde", modelo: "urbana" });
            Bici.add(help, function (err, newBici) {
                if (err) console.log(err);
                Bici.allBicis(function (err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toEqual(help.code);
                    done();
                });
            });
        });
    });

    describe('Bici.findByCode', () => {
        it('Devolver la Bici con code 1', (done) => {
            Bici.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                var aBici = new Bici({ code: 1, color: "Verde", modelo: "urbana" });
                Bici.add(aBici, function (err, newBici) {
                    if (err) console.log(err);
                    var bBici = new Bici({ code: 2, color: "Rojo", modelo: "4x4" });
                    Bici.add(bBici, function (error, newBici) {
                        if (error) console.log(error);
                        Bici.findByCode(1, function (err, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        });

                    });
                });
            });
        });
    });

    describe('Bici.removeByCode', () => {
        it('Debe borrar la bici con code 1', (done) => {
            Bici.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                var a = new Bici({ code: 1, color: 'blanca', modelo: 'montaña' });
                Bici.add(a, function (err, newBici) {
                    if (err) console.log(err);

                    var b = new Bici({ code: 2, color: 'roja', modelo: 'urbana' });
                    Bici.add(b, function (err, newBici) {
                        if (err) console.log(err);
                        // Borramos el code=1
                        Bici.removeByCode(1, function (err, bici) {
                            if (err) console.log(err);
                            // Buscamos el que acabamos de borrar
                            Bici.findByCode(1, function (err, bici) {
                                if (err) console.log(err);
                                // La busqueda debera devolver null
                                expect(bici).toBe(null);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    describe('UpdateByCode', () => {
        it('actualiza datos bici', (done) => {
            Bici.allBicis(function (err, bicis) {
                if (err) console.log(err);
                expect(bicis.length).toBe(0);
                var a = new Bici({ code: 1, color: 'blanca', modelo: 'montaña' });
                Bici.add(a, function (err, newBici) {
                    if (err) console.log(err);
                    Bici.allBicis(function (err, bicis) {
                        if (err) console.log(err);
                        expect(bicis.length).toBe(1);
                        Bici.findByCode(1, function (err, bici) {
                            if (err) console.log(err);
                            expect(bici.color).toBe('blanca');
                            var b = new Bici({ code: 1, color: 'roja', modelo: 'ciudad' });
                            Bici.updateByCode(b, () => {
                                Bici.findByCode(1, function (err, bici) {
                                    if (err) console.log(err);
                                    expect(bici.code).toBe(1);
                                    expect(bici.color).toBe('blanca');
                                    expect(bici.modelo).toBe('montaña');
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});
