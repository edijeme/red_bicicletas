var mongoose = require('mongoose');
var Bici = require('../../models/bicicletaMdl');
var server = require('../../bin/www');
var request = require('request');

var base_url = "http://localhost:5000/api/bicicletas";

describe('Bicicleta API', () => {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('Conexion Exitosa a la base de datos test');
            done();
        });
    });

    afterEach(function (done) {
        Bici.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });
    /*
        describe('GET BICICLETA API /', () => {
            it('Status 200', (done) => {
                request.get(base_url, function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body);
                    console.log(bici);
                    expect(bici.length).toBe(0);
                    done();
                });
            });
        });
    */
    describe('POST BICICLETA API /create', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var data = '{"id": 1, "color":"Rojo", "modelo": "Urbana", "lat":10.46, "lng":-73.24}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: data
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicletas;
                expect(bici.color).toBe("Rojo");
                expect(bici.ubicacion[0]).toBe(10.46);
                expect(bici.ubicacion[1]).toBe(-73.24);
                done();
            });
        });
    });
    /*
        describe('PUT BICICLETA API /update', () => {
            it('Status 200', (done) => {
                var headers = { 'content-type': 'application/json' };
                var data = '{"id":1,"color":"marron","modelo":"4x4","lat":10.48,"lng":-73.24}';
    
                request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/create',
                    body: data
                }, function (error, response, body) {
                    expect(response.statusCode).toBe(200);
    
                    var dat = '{"id_old": 1, "id":5,"color":"Negro","modelo":"2x4","lat":10.48,"lng":-73.24}';
    
                    request.put({
                        headers: headers,
                        url: 'http://localhost:5000/api/bicicletas/update',
                        body: dat
                    }, function (error, response, body) {
                        expect(response.statusCode).toBe(200);
                        var res = JSON.parse(body).bicicleta;
                        expect(res.color).toBe("Negro");
                        done();
                    });
                });
            });
        });
    
        describe('DELETE BICICLETA API /delete', () => {
            it('Status 200', (done) => {
                var headers = { 'content-type': 'application/json' };
                var data = '{"id":1,"color":"marron","modelo":"4x4","lat":10.48,"lng":-73.24}';
    
                request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/create',
                    body: data
                }, function (error, response, body) {
                    expect(response.statusCode).toBe(200);
    
                    request.delete({
                        headers: headers,
                        url: 'http://localhost:5000/api/bicicletas/delete',
                        body: data
                    }, function (error, response, body) {
                        expect(response.statusCode).toBe(204);
                        done();
                    });
                });
            });
        });
        */
});
