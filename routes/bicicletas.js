var express = require('express');
var router = express.Router();
var biciCtrl = require('../controllers/bicicletaCtrl')

router.get('/', biciCtrl.bicicleta_list);
router.get('/create', biciCtrl.bicicleta_create_get);
router.post('/create', biciCtrl.bicicleta_create_post);
router.get('/:id/update', biciCtrl.bicicleta_update_get);
router.post('/:id/update', biciCtrl.bicicleta_update_post);
router.post('/:id/delete', biciCtrl.bicicleta_delete_post);

module.exports = router;