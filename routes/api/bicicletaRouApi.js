var express = require('express');
var router = express.Router();
var biciCtrl = require('../../controllers/api/bicicletaCtrlApi');

router.get('/', biciCtrl.bici_list);
router.post('/create', biciCtrl.bici_insert);
router.put('/update', biciCtrl.bici_update);
router.delete('/delete', biciCtrl.bici_delete);
module.exports = router;