var express = require('express');
var router = express.Router();
const userCtrl = require('../controllers/usuariosCtrl');

router.get('/', userCtrl.list);
router.get('/create', userCtrl.create_get);
router.post('/create', userCtrl.create);
router.get('/:id/update', userCtrl.update_get);
router.post('/:id/update', userCtrl.update);
router.post('/:id/delete', userCtrl.delete);

module.exports = router;